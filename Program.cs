﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        const string EMERGENCY_POWER_TAG = "[Emergency Power]";
        const int ACTIVATE_EMERGENCY_POWER_THRESHOLD = 10;
        const int DEACTIVATE_EMERGENCY_POWER_THRESHOLD = 80;

        private bool lastEnabledStatus = false;
        private int activations = 0;

        private List<IMyPowerProducer> powerProducers;
        private List<IMyBatteryBlock> batteryBlocks;

        public Program()
        {
            this.Runtime.UpdateFrequency = UpdateFrequency.Update100;
            InitBlocks();
        }

        private void InitBlocks()
        {
            powerProducers = new List<IMyPowerProducer>();
            GridTerminalSystem.GetBlocksOfType(powerProducers, b => isEmergencyPowerObject(b) && !(b is IMyBatteryBlock));

            batteryBlocks = new List<IMyBatteryBlock>();
            GridTerminalSystem.GetBlocksOfType(batteryBlocks, b => isEmergencyPowerObject(b));
        }

        public void Main(string arguments,UpdateType updateSource)
        {
            if (updateSource == UpdateType.Terminal)
            {
                // simple execution in terminal reloads blocks
                InitBlocks();
            }
            if (updateSource != UpdateType.Update100)
            {
                return;
            }

            Echo("Activation threshold: \t" + ACTIVATE_EMERGENCY_POWER_THRESHOLD + "%");
            Echo("Deactivation threshold: \t" + DEACTIVATE_EMERGENCY_POWER_THRESHOLD + "%");
            Echo("Emergency Power: \t" + (lastEnabledStatus ? "On" : "Off"));

            if (ACTIVATE_EMERGENCY_POWER_THRESHOLD >= DEACTIVATE_EMERGENCY_POWER_THRESHOLD)
            {
                Echo("Activation threshold is higher than deactivation threshold! This will keep the emergency Power active");
            }

            if (batteryBlocks.Count == 0)
            {
                Echo("No Batteries found to monitor. Add '" + EMERGENCY_POWER_TAG + "' in the Custom Info");
                return;
            }
            if (powerProducers.Count == 0)
            {
                Echo("No Power Producers found. Add '" + EMERGENCY_POWER_TAG + "' in the Custom Info");
                return;
            }

            var currentPower = 0f;
            var maximumPower = 0f;
            foreach (var batteryBlock in batteryBlocks)
            {
                currentPower += batteryBlock.CurrentStoredPower;
                maximumPower += batteryBlock.MaxStoredPower;
            }

            var percentageStored = Convert.ToInt32((currentPower / maximumPower) * 100);

            Echo("Stored power: " + percentageStored + "% in " + batteryBlocks.Count + " Batteries");
            Echo("Found emergency power producers:");

            foreach (var powerProducer in powerProducers)
            {
                Echo("- " + powerProducer.CustomName);
            }

            Echo("Activated " + activations + " times");

            bool enablePowerProducers;

            if (percentageStored < ACTIVATE_EMERGENCY_POWER_THRESHOLD)
            {
                enablePowerProducers = true;
            }
            else if (percentageStored > DEACTIVATE_EMERGENCY_POWER_THRESHOLD)
            {
                enablePowerProducers = false;
            }
            else
            {
                return;
            }

            if (enablePowerProducers != lastEnabledStatus)
            {
                lastEnabledStatus = enablePowerProducers;
                if (enablePowerProducers)
                    activations++;

                foreach (var powerProducer in powerProducers)
                {
                    powerProducer.Enabled = enablePowerProducers;
                }
            }
        }

        private bool isEmergencyPowerObject(IMyTerminalBlock block)
        {
            return block.CustomData.Contains(EMERGENCY_POWER_TAG);
        }
    }
}
